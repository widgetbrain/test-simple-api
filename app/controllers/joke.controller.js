const axios = require('axios');

module.exports = function main(app) {
  const { Joke } = app.models;

  // CRUD

  Joke.remoteMethod('getJoke', {
    http: { path: '/:id', verb: 'get' },
    accepts: { arg: 'id', type: 'string' },
    returns: { root: true },
  });
  Joke.getJoke = async (id) => Joke.findById(id);

  Joke.remoteMethod('getJokes', {
    http: { path: '/', verb: 'get' },
    returns: { root: true },
  });
  Joke.getJokes = async () => Joke.find({});

  Joke.remoteMethod('createJoke', {
    http: { path: '/', verb: 'post' },
    accepts: [{ arg: 'data', type: 'object', http: { source: 'body' } }],
    returns: { root: true },
  });
  Joke.createJoke = async (data) => Joke.create(data);

  Joke.remoteMethod('updateJoke', {
    http: { path: '/', verb: 'put' },
    accepts: [{ arg: 'data', type: 'object', http: { source: 'body' } }],
    returns: { root: true },
  });
  Joke.updateJoke = async (data) => Joke.upsert(data);

  Joke.remoteMethod('deleteJoke', {
    http: { path: '/:id', verb: 'delete' },
    accepts: { arg: 'id', type: 'string' },
    returns: { root: true },
  });
  Joke.deleteJoke = async (id) => Joke.deleteById(id);

  Joke.remoteMethod('fetchNewJoke', {
    http: { path: '/new', verb: 'get' },
    returns: { root: true },
  });
  Joke.fetchNewJoke = async () => {
    const sourceUrl = 'http://api.icndb.com/jokes/random/';
    const res = axios(sourceUrl, {
      method: 'get',
    });
    const { data } = res;

    return Joke.createJoke({
      text: data.value.joke,
    });
  };

  // TEXT

  Joke.remoteMethod('analyseWords', {
    http: { path: '/analyse/words/:id', verb: 'get' },
    accepts: { arg: 'id', type: 'string' },
    returns: { root: true },
  });
  Joke.analyseWords = async (id) => {
    const joke = await Joke.findById(id);
    const words = joke.text.split(' ');
    return {
      words,
    };
  };

  Joke.remoteMethod('analyseWordsAll', {
    http: { path: '/analyse/words', verb: 'get' },
    returns: { root: true },
  });
  Joke.analyseWordsAll = async () => {
    const jokes = await Joke.find({});
    const words = jokes.map((joke) => joke.text.split(' '));
    return {
      words,
    };
  };
};
