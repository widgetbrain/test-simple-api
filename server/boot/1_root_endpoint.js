/**
 * Install a `/` route that returns the server status
 */
module.exports = function root(app) {
  const router = app.loopback.Router();
  router.get('/', app.loopback.status());
  app.use(router);
};
