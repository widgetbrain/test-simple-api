require('chai').should();
const { initServer } = require('../utils');

describe('joke', () => {
  let app;
  let server;

  before(async () => {
    ({ server, app } = await initServer());
  });

  context('CRUD', () => {
    it('should get joke', async () => {
      const res = await server.get('/api/jokes/1').expect(200);

      res.body.should.have.property('id', 1);
      res.body.should.have.property(
        'text',
        'How does a computer get drunk? It takes screenshots.',
      );
    });

    it('should list jokes', async () => {
      const res = await server.get('/api/jokes').expect(200);

      res.body.length.should.equal(10);
    });

    it('should store new joke', async () => {
      const res = await server
        .post('/api/jokes')
        .send({ text: 'new joke' })
        .expect(200);
      const joke = await app.models.Joke.findById(res.body.id);
      await app.models.Joke.deleteById(res.body.id);

      res.body.should.have.property('id');
      joke.should.have.property('id', res.body.id);
      joke.should.have.property('text', res.body.text);
    });

    it('should update joke', async () => {
      const joke = await app.models.Joke.create({
        text: 'joke to be updated',
      });

      await server
        .put('/api/jokes')
        .send({
          id: joke.id,
          text: 'joke that was updated',
        })
        .expect(200);
      const res = await server.get(`/api/jokes/${joke.id}`).expect(200);
      await app.models.Joke.deleteById(res.body.id);

      res.body.should.have.property('id', joke.id);
      res.body.should.have.property('text', 'joke that was updated');
    });

    it('should delete joke', async () => {
      const joke = await app.models.Joke.create({
        text: 'joke to be deleted',
      });

      const resBefore = await server.get(`/api/jokes/${joke.id}`).expect(200);
      const resDel = await server.delete(`/api/jokes/${joke.id}`).expect(200);
      const resAfter = await server.get(`/api/jokes/${joke.id}`).expect(200);

      resBefore.body.should.have.property('id', joke.id);
      resDel.body.should.have.property('count', 1);
      resAfter.should.have.property('body', null);
    });

    it('[TO FIX] should not store duplicate jokes', async () => {
      const resFirst = await server
        .post('/api/jokes')
        .send({ text: 'same old joke' })
        .expect(200);
      const resSecond = await server
        .post('/api/jokes')
        .send({ text: 'same old joke' })
        .expect(200);
      await app.models.Joke.deleteById(resFirst.body.id);

      resFirst.body.should.have.property('id');
      resSecond.body.should.have.property('id', resFirst.body.id);
    });

    it('[TO FIX] should fetch new joke from source', async () => {
      const res = await server.get('/api/jokes/new').expect(200);
      await app.models.Joke.deleteById(res.body.id);

      res.body.should.have.property('id');
      res.body.should.have.property('text');
    });
  });

  context('text analysis', () => {
    it('[TO FIX] should analyse words from a joke', async () => {
      const res1 = await server.get(`/api/jokes/analyse/words/${1}`).expect(200);
      const words1 = res1.body.words;

      words1.should.contain('how');
      words1.should.contain('drunk');
      words1.should.contain('screenshots');

      const res6 = await server.get(`/api/jokes/analyse/words/${6}`).expect(200);
      const words6 = res6.body;

      words6.should.contain('programmer');
      words6.should.contain('while');
      words6.should.contain('milk');
    });

    it('[TO FIX] should analyse words from all stored jokes', async () => {
      const res = await server.get('/api/jokes/analyse/words').expect(200);
      const { words } = res.body;

      words.should.contain('how');
      words.should.contain('drunk');
      words.should.contain('screenshots');
      words.should.contain('programmer');
      words.should.contain('while');
      words.should.contain('milk');
    });
  });
});
